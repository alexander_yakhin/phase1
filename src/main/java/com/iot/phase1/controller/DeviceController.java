/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iot.phase1.controller;

import com.iot.phase1.Exception.ResourceNotFoundException;
import com.iot.phase1.model.Device;
import com.iot.phase1.repository.DeviceRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author A650322
 */
@RestController

public class DeviceController {
    @RequestMapping("/api/v1")
    public Device newDevice(){
        return new Device(1,"Sensor",65654,"Temp","TEst");
    }
    @Autowired
    private DeviceRepository deviceRepository;

    @GetMapping("/devices")
    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    @GetMapping("/devices/{id}")
    public ResponseEntity<Device> getDeviceById(@PathVariable(value = "id") Integer deviceId)
        throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
          .orElseThrow(() -> new ResourceNotFoundException("Device not found for this id :: " + deviceId));
        return ResponseEntity.ok().body(device);
    }
    
    @PostMapping("/devices")
    public Device createDevice(@Valid @RequestBody Device device) {
        return deviceRepository.save(device);
    }

    @PutMapping("/devices/{id}")
    public ResponseEntity<Device> updateDevice (@PathVariable(value = "id") int deviceId,
                                                @Valid @RequestBody Device deviceDetails) throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
                        .orElseThrow(() -> new ResourceNotFoundException("Device not found for this id :: " + deviceId));

        device.setId(deviceId);
        device.setName(deviceDetails.getName());
        device.setSn(deviceDetails.getSn());
        device.setType(deviceDetails.getType());
        final Device updatedDevice = deviceRepository.save(device);
        return ResponseEntity.ok(updatedDevice);
    }
 @DeleteMapping("/devices/{id}")
    public Map<String, Boolean> deleteDevice(@PathVariable(value = "id") Integer deviceId)
         throws ResourceNotFoundException {
        Device device = deviceRepository.findById(deviceId)
       .orElseThrow(() -> new ResourceNotFoundException("Device not found for this id :: " + deviceId));

        deviceRepository.delete(device);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
