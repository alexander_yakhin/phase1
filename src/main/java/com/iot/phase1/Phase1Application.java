package com.iot.phase1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class Phase1Application {

	public static void main(String[] args) {
		SpringApplication.run(Phase1Application.class, args);
	}

}
