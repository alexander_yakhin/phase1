/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iot.phase1.model;

/**
 *
 * @author A650322
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Devices")
public class Device {
    
    
    private int id ;
    private String name;  
    private long sn;
    private String type;
    private String version;

    
    public Device() {
    }

    public Device(int id, String name, long sn, String type, String version) {
        this.id = id;
        this.name = name;
        this.sn = sn;
        this.type = type;
        this.version = version;
    }
    @Id
     @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   @Column( name = "sn", nullable = false)
    public long getSn() {
        return sn;
    }

    public void setSn(long sn) {
        this.sn = sn;
    }
   @Column(name = "type", nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
   @Column(name = "version", nullable = false)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    
    
           
    
}
