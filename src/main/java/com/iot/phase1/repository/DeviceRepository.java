/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iot.phase1.repository;

import com.iot.phase1.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author A650322
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device,Integer> {
    
}
